package com.bisphone.interviewtest.interfaces;

import com.bisphone.interviewtest.model.Contact;

import java.util.List;

public interface ContactListWebApiListener {
    void contactListAchieved(List<Contact> items);
    void errorHappened(String error);
}
