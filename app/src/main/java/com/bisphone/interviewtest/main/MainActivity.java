package com.bisphone.interviewtest.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bisphone.interviewtest.R;
import com.bisphone.interviewtest.adapter.ContactListAdapter;
import com.bisphone.interviewtest.adapter.SearchListAdapter;
import com.bisphone.interviewtest.custom.CallNumberField;
import com.bisphone.interviewtest.custom.FabRevealLayoutCustom.FABRevealLayoutCustom;
import com.bisphone.interviewtest.model.Contact;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View {
    @BindView(R.id.fab_reveal_layout)
    FABRevealLayoutCustom fabRevealLayout;
    @BindView(R.id.textView_main_dial_1)
    AppCompatTextView tvDial1;
    @BindView(R.id.textView_main_dial_2)
    AppCompatTextView tvDial2;
    @BindView(R.id.textView_main_dial_3)
    AppCompatTextView tvDial3;
    @BindView(R.id.textView_main_dial_4)
    AppCompatTextView tvDial4;
    @BindView(R.id.textView_main_dial_5)
    AppCompatTextView tvDial5;
    @BindView(R.id.textView_main_dial_6)
    AppCompatTextView tvDial6;
    @BindView(R.id.textView_main_dial_7)
    AppCompatTextView tvDial7;
    @BindView(R.id.textView_main_dial_8)
    AppCompatTextView tvDial8;
    @BindView(R.id.textView_main_dial_9)
    AppCompatTextView tvDial9;
    @BindView(R.id.textView_main_dial_0)
    AppCompatTextView tvDial0;
    @BindView(R.id.textView_main_dial_plus)
    AppCompatTextView tvDialPlus;
    @BindView(R.id.textView_main_dial_del)
    AppCompatTextView tvDialDel;
    @BindView(R.id.callNumberFiled_main)
    CallNumberField callNumberField;
    @BindView(R.id.recyclerView_main_contactList)
    RecyclerView rvContacts;
    @BindView(R.id.recyclerView_main_searchList)
    RecyclerView rvSearch;
    @BindView(R.id.swipeLayout_main)
    SwipeRefreshLayout swipeView;
    @BindView(R.id.view_main_error)
    View errorView;
    @BindView(R.id.textView_main_error)
    TextView tvError;
    Boolean isShown = false;

    private MainContract.Presenter presenter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
        presenter.viewCreated();
    }

    @Override
    public void hideAllDialButtons() {
        tvDial0.setVisibility(View.INVISIBLE);
        tvDial1.setVisibility(View.INVISIBLE);
        tvDial2.setVisibility(View.INVISIBLE);
        tvDial3.setVisibility(View.INVISIBLE);
        tvDial4.setVisibility(View.INVISIBLE);
        tvDial5.setVisibility(View.INVISIBLE);
        tvDial6.setVisibility(View.INVISIBLE);
        tvDial7.setVisibility(View.INVISIBLE);
        tvDial8.setVisibility(View.INVISIBLE);
        tvDial9.setVisibility(View.INVISIBLE);
        tvDial0.setVisibility(View.INVISIBLE);
        tvDialPlus.setVisibility(View.INVISIBLE);
        tvDialDel.setVisibility(View.INVISIBLE);
    }


    public void animateSingleButton(View v,int delay){
        new Handler().postDelayed(() -> {
            v.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.ZoomIn).playOn(v);
        }, delay * 20);
    }

    @Override
    public void animateDialButtons(){
        animateSingleButton(tvDial1,0);
        animateSingleButton(tvDial2,1);
        animateSingleButton(tvDial3,2);
        animateSingleButton(tvDial4,3);
        animateSingleButton(tvDial5,4);
        animateSingleButton(tvDial6,5);
        animateSingleButton(tvDial7,6);
        animateSingleButton(tvDial8,7);
        animateSingleButton(tvDial9,8);
        animateSingleButton(tvDialPlus,9);
        animateSingleButton(tvDial0,10);
        animateSingleButton(tvDialDel,11);
    }

    @Override
    public void addNewFieldToCallNumber(String number) {
        callNumberField.addFieldAtTheEnd(number);
    }

    @Override
    public void removeFieldFromCallNumber() {
        callNumberField.removeLastItem();
    }

    private void showMainFabLayout(){
        fabRevealLayout.revealMainView();
        setSearchListItems(new ArrayList<>()); // clear list
        hideAllDialButtons();
        callNumberField.reset();
    }

    @OnClick({R.id.textView_main_dial_1,
            R.id.textView_main_dial_2,
            R.id.textView_main_dial_3,
            R.id.textView_main_dial_4,
            R.id.textView_main_dial_5,
            R.id.textView_main_dial_6,
            R.id.textView_main_dial_7,
            R.id.textView_main_dial_8,
            R.id.textView_main_dial_9,
            R.id.textView_main_dial_0,
            R.id.textView_main_dial_plus,
            R.id.textView_main_dial_del,
            R.id.button_main_retry,
    R.id.button_main_call, R.id.main_call_fab})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.textView_main_dial_0: presenter.dialButtonsClicked("0"); break;
            case R.id.textView_main_dial_1: presenter.dialButtonsClicked("1"); break;
            case R.id.textView_main_dial_2: presenter.dialButtonsClicked("2"); break;
            case R.id.textView_main_dial_3: presenter.dialButtonsClicked("3"); break;
            case R.id.textView_main_dial_4: presenter.dialButtonsClicked("4"); break;
            case R.id.textView_main_dial_5: presenter.dialButtonsClicked("5"); break;
            case R.id.textView_main_dial_6: presenter.dialButtonsClicked("6"); break;
            case R.id.textView_main_dial_7: presenter.dialButtonsClicked("7"); break;
            case R.id.textView_main_dial_8: presenter.dialButtonsClicked("8"); break;
            case R.id.textView_main_dial_9: presenter.dialButtonsClicked("9"); break;
            case R.id.textView_main_dial_plus: presenter.dialButtonsClicked("+"); break;
            case R.id.textView_main_dial_del:
                presenter.deleteDialButtonClicked();
                break;
            case R.id.button_main_call:
               callNumber(getEnteredPhoneNumber());
                break;
            case R.id.button_main_retry:
                showSwipeAnimation();
                hideErrorView();
                presenter.swipeViewRefreshed();
                break;
            case R.id.main_call_fab:
                if(isShown)
                    showMainFabLayout();
                else
                    fabRevealLayout.revealSecondaryView();
                isShown=!isShown;
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if(isShown){
            showMainFabLayout();
            isShown=!isShown;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setupUI() {
        fabRevealLayout.setOnRevealChangeListener(new FABRevealLayoutCustom.OnRevealChangeListener() {
            @Override
            public void onMainViewAppeared(FABRevealLayoutCustom fabRevealLayout, View mainView) {
                presenter.fabRevealMainViewAppeared();
            }

            @Override
            public void onSecondaryViewAppeared(FABRevealLayoutCustom fabRevealLayout, View secondaryView) {
                presenter.fabRevealSecondaryViewAppeared();

            }
        });

        rvContacts.setLayoutManager(new LinearLayoutManager(getContext()));
        rvContacts.setAdapter(new ContactListAdapter(view -> {
            Contact contact = (Contact) view.getTag();
            presenter.listContactItemClicked(contact);
        }));
        rvSearch.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        rvSearch.setAdapter(new SearchListAdapter(view->{
            Contact contact = (Contact) view.getTag();
            presenter.listContactItemClicked(contact);
        }));
    }

    @Override
    public void setListItems(List<Contact> contactList) {
        ((ContactListAdapter)rvContacts.getAdapter()).setNewData(contactList);
    }

    @Override
    public void showErrorView(String error) {
        tvError.setText(error);
        errorView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.FadeInDown).playOn(errorView);
    }

    @Override
    public void showSwipeAnimation() {
        swipeView.setRefreshing(true);
    }

    @Override
    public void setupSwipeView(int... color) {
        swipeView.setColorSchemeResources(color);
        swipeView.setOnRefreshListener(() -> {
            presenter.swipeViewRefreshed();
        });
    }

    @Override
    public void stopSwipeAnimation() {
        swipeView.setRefreshing(false);
    }

    @Override
    public String getEnteredPhoneNumber() {
        return callNumberField.getCallNumber();
    }

    @Override
    public void setSearchListItems(List<Contact> searchContacts) {
        ((SearchListAdapter)rvSearch.getAdapter()).setNewData(searchContacts);
    }

    @Override
    public void callNumber(String number) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + number));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    @Override
    public void hideErrorView() {
        errorView.setVisibility(View.GONE);
    }

    private Context getContext(){
        return this;
    }
}
