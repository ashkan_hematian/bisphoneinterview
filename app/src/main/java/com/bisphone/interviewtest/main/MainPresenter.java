package com.bisphone.interviewtest.main;

import com.bisphone.interviewtest.R;
import com.bisphone.interviewtest.model.Contact;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class MainPresenter implements MainContract.Presenter,MainContract.ModelPresenter{

    private final MainContract.Model model;
    private final MainContract.View view;

    public MainPresenter(MainContract.View view) {
        this.view = view;
        this.model = new MainModel(this);
    }

    @Override
    public void viewCreated() {
        view.setupSwipeView(R.color.md_red_400, R.color.md_blue_400, R.color.md_green_400, R.color.md_orange_400);
        view.setupUI();
        List<Contact> contacts = model.getContactListFromSharedPreference();
        if(contacts==null || contacts.size() == 0){
            view.showSwipeAnimation();
            model.fetchContactListFromServer();
        } else {
            view.setListItems(contacts);
        }
    }

    @Override
    public void swipeViewRefreshed() {
        view.hideErrorView();
        model.fetchContactListFromServer();
    }

    @Override
    public void NumberChanged() {
        final String number = view.getEnteredPhoneNumber();
        List<Contact> contacts = model.getContactListFromSharedPreference();
        Collection<Contact> searchedContacts =  CollectionUtils.select(contacts, new Predicate<Contact>() {
            @Override
            public boolean evaluate(Contact object) {
                if(object.getPhone().contains(number))
                    return true;
                return false;
            }
        });
        view.setSearchListItems(new ArrayList<>(searchedContacts));
    }

    @Override
    public void listContactItemClicked(Contact contact) {
        view.callNumber(contact.getPhone());
    }

    @Override
    public void fabRevealMainViewAppeared() {
        view.hideAllDialButtons();
    }

    @Override
    public void fabRevealSecondaryViewAppeared() {
        view.animateDialButtons();
        NumberChanged();
    }

    @Override
    public void dialButtonsClicked(String number) {
        view.addNewFieldToCallNumber(number);
        NumberChanged();
    }

    @Override
    public void deleteDialButtonClicked() {
        view.removeFieldFromCallNumber();
        NumberChanged();
    }

    @Override
    public void webApiContactListErrorHappened(String error) {
        view.stopSwipeAnimation();
        view.setListItems(new ArrayList<>());
        view.showErrorView(error);
    }

    @Override
    public void webApiContactListSuccessfulResult(List<Contact> contactList) {
        view.setListItems(contactList);
        model.saveContactsInSharedPreference(contactList);
        view.stopSwipeAnimation();
    }
}
