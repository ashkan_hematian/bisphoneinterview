package com.bisphone.interviewtest.main;

import com.bisphone.interviewtest.connection.ContactListWebApi;
import com.bisphone.interviewtest.interfaces.ContactListWebApiListener;
import com.bisphone.interviewtest.model.Contact;
import com.bisphone.interviewtest.utility.PreferenceHelper;

import java.util.List;

class MainModel implements MainContract.Model{

    private final MainContract.ModelPresenter presenter;

    public MainModel(MainContract.ModelPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void fetchContactListFromServer() {
        ContactListWebApi webApi= new ContactListWebApi();
        webApi.setWebApiListener(new ContactListWebApiListener() {
            @Override
            public void contactListAchieved(List<Contact> items) {
                presenter.webApiContactListSuccessfulResult(items);
            }

            @Override
            public void errorHappened(String error) {
                presenter.webApiContactListErrorHappened(error);
            }
        });
        webApi.fetchContactList();
    }

    @Override
    public List<Contact> getContactListFromSharedPreference() {
        return PreferenceHelper.getInstance().getContactList();
    }

    @Override
    public void saveContactsInSharedPreference(List<Contact> contactList) {
        PreferenceHelper.getInstance().setContactList(contactList);
    }
}
