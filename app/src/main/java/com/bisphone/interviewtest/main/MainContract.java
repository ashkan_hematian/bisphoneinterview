package com.bisphone.interviewtest.main;

import com.bisphone.interviewtest.model.Contact;

import java.util.List;

class MainContract {

    interface View{
        void setupUI();
        void setListItems(List<Contact> contactList);
        void showErrorView(String error);
        void showSwipeAnimation();
        void setupSwipeView(int... color);
        void stopSwipeAnimation();
        String getEnteredPhoneNumber();
        void setSearchListItems(List<Contact> searchContacts);
        void callNumber(String number);
        void hideErrorView();
        void hideAllDialButtons();
        void animateDialButtons();
        void addNewFieldToCallNumber(String number);
        void removeFieldFromCallNumber();
    }

    interface Presenter{
        void viewCreated();
        void swipeViewRefreshed();
        void NumberChanged();
        void listContactItemClicked(Contact contact);
        void fabRevealMainViewAppeared();
        void fabRevealSecondaryViewAppeared();
        void dialButtonsClicked(String number);
        void deleteDialButtonClicked();
    }

    interface Model{
        void fetchContactListFromServer();
        List<Contact> getContactListFromSharedPreference();
        void saveContactsInSharedPreference(List<Contact> contactList);
    }

    interface ModelPresenter{
        void webApiContactListErrorHappened(String error);
        void webApiContactListSuccessfulResult(List<Contact> contactList);
    }
}
