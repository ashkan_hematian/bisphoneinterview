package com.bisphone.interviewtest.base;

import android.app.Application;
import android.content.Context;

public class MasterApplication extends Application {

    private static MasterApplication instance ;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }
}
