package com.bisphone.interviewtest.connection;

import com.bisphone.interviewtest.interfaces.ContactListWebApiListener;
import com.bisphone.interviewtest.model.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class ContactListWebApi {

    private final String BASE_URL = "http://hkns.ir/";
    private ContactListWebApiListener listener;

    public void fetchContactList(){
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();

       retrofit.create(ContactApi.class).getAll().enqueue(new Callback<List<Contact>>() {
           @Override
           public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
               if(listener!=null)
                   listener.contactListAchieved(response.body());
           }

           @Override
           public void onFailure(Call<List<Contact>> call, Throwable t) {
               if(listener!=null)
                   listener.errorHappened("اشکال در ارتباط با سرور");
           }
       });

    }


    interface ContactApi {
        @GET("contacts.php")
        Call<List<Contact>> getAll();
    }

    public void setWebApiListener(ContactListWebApiListener listener) {
        this.listener = listener;
    }
}
