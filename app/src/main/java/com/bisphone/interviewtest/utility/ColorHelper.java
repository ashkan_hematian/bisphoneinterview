package com.bisphone.interviewtest.utility;

import android.support.v4.content.ContextCompat;

import com.bisphone.interviewtest.R;
import com.bisphone.interviewtest.base.MasterApplication;

public class ColorHelper {

    private final static int[] colorList = {
            R.color.md_green_300,
            R.color.md_red_300,
            R.color.md_blue_300,
            R.color.md_orange_300,
            R.color.md_deep_purple_300,
            R.color.md_yellow_300
    };

    public static int getColor(int position){
        return ContextCompat.getColor(MasterApplication.getContext(),colorList[position%colorList.length]);
    }
}
