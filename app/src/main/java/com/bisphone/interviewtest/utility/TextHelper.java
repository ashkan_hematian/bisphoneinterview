package com.bisphone.interviewtest.utility;

public class TextHelper {

    public static String getCircleDrawableContactName(String txt){
        if(txt == null || txt.trim().equals(""))
            return "";
        if(txt.length()>2)
            return txt.substring(0,2);
        else
            return txt;
    }
}
