package com.bisphone.interviewtest.utility;

public class FarsiNumberHelper {

    public static String fixArabicNumber(String txt){
        txt=txt.replaceAll("۱","1");
        txt=txt.replaceAll("۲","2");
        txt=txt.replaceAll("۳","3");
        txt=txt.replaceAll("۴","4");
        txt=txt.replaceAll("۵","5");
        txt=txt.replaceAll("۶","6");
        txt=txt.replaceAll("۷","7");
        txt=txt.replaceAll("۸","8");
        txt=txt.replaceAll("۹","9");
        txt=txt.replaceAll("۰","0");
        return txt;
    }
}
