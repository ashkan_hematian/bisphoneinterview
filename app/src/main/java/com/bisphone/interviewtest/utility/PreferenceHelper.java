package com.bisphone.interviewtest.utility;

import android.content.SharedPreferences;

import com.bisphone.interviewtest.base.MasterApplication;
import com.bisphone.interviewtest.model.Contact;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PreferenceHelper {

    private static PreferenceHelper instance;
    private SharedPreferences sf;

    private final String CONTACT_LIST = "cList";

    private static class InstanceHelper {
        protected static final PreferenceHelper instance = new PreferenceHelper();
    }

    private PreferenceHelper(){}

    public static PreferenceHelper getInstance(){
        if(instance==null) {
            instance = InstanceHelper.instance;
            instance.sf = MasterApplication.getContext().getSharedPreferences("InterviewTest",0);
        }
        return instance;
    }

    public void setContactList(List<Contact> items){
        Gson gson = new Gson();
        Type type=new TypeToken<List<Contact>>(){}.getType();
        String data = gson.toJson(items);
        sf.edit().putString(CONTACT_LIST,data).apply();
    }

    public List<Contact> getContactList(){
        ArrayList<Contact> contacts=new ArrayList<>();
        try {
            String data = sf.getString(CONTACT_LIST, "[]");
            Gson gson = new Gson();
            JSONArray jArray = new JSONArray(data);
            for (int i = 0; i < jArray.length(); i++) {
                contacts.add(gson.fromJson(jArray.getJSONObject(i).toString(), Contact.class));
            }
        } catch (Exception e){

        }
        return contacts;
    }
}
