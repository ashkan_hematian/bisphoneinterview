package com.bisphone.interviewtest.custom;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bisphone.interviewtest.utility.FarsiNumberHelper;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.List;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

public class CallNumberField  extends FrameLayout{

    final List<String> values = new ArrayList<>();
    int textColor = Color.BLACK ;
    int paddingText = 10;
    Boolean isLock = false;
    Boolean isCopyPasteViewShown = false;
    private LinearLayout numberFields;
    private CallNumberCopyPasteView copyPasteView;

    public CallNumberField(Context context) {
        super(context);
        invalidateParameters();
    }

    public CallNumberField(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        invalidateParameters();
    }

    public CallNumberField(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        invalidateParameters();
    }

    private void invalidateParameters() {
        numberFields = new LinearLayout(getContext());
        numberFields.setGravity(Gravity.CENTER);
        copyPasteView = new CallNumberCopyPasteView(getContext());
        this.addView(numberFields);
        this.addView(copyPasteView);
        this.setOnLongClickListener(view -> {
            if(isCopyPasteViewShown == false)
                enterCopyPasteMode();
            else
                exitCopyPasteMode();
            return true;
        });
        copyPasteView.setButtonClickListener(new CallNumberCopyPasteView.ButtonClickListener() {
            @Override
            public void copyClicked() {
                exitCopyPasteMode();

                ClipboardManager clipboard = (ClipboardManager)
                        getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Number", getCallNumber());
                clipboard.setPrimaryClip(clip);

            }

            @Override
            public void pasteClicked() {
                exitCopyPasteMode();
                ClipboardManager clipboard = (ClipboardManager)
                        getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                if(clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN)) {
                    String text = clipboard.getPrimaryClip().getItemAt(0).getText().toString();
                    text = FarsiNumberHelper.fixArabicNumber(text);
                    for(int i=0;i<text.length();i++){
                        String txtNumber = text.substring(i,i+1);
                        switch (txtNumber){
                            case "1":
                            case "2":
                            case "3":
                            case "4":
                            case "5":
                            case "6":
                            case "7":
                            case "8":
                            case "9":
                            case "0":
                            case "+":
                                addFieldWithoutAnimation(txtNumber);
                                break;
                        }
                    }

                }
            }
        });
    }

    private void addFieldWithoutAnimation(String value) {
        addField(values.size(),value,false);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CallNumberField(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        invalidateParameters();
    }

    public synchronized void removeField(int position){
        if(isLock)
            return;
        if(isCopyPasteViewShown) {
            exitCopyPasteMode();
            return;
        }
        if(values.size()==0)
            return;
        if(position>=values.size())
            return;
        isLock = true ;
        values.remove(position);
        animateRemoveChild(position);
    }

    public synchronized void removeLastItem(){
        removeField(getChildSize()-1);
    }

    public void addFieldAtTheEnd(String value){
        addField(values.size(),value,true);
    }

    public synchronized void addField(int position, String value,Boolean showAnimation){
        if(isLock)
            return;
        if(isCopyPasteViewShown) {
            exitCopyPasteMode();
            return;
        }
        if(position > values.size())
            return;

        isLock = true;
        values.add(position,value);
        animateAddChild(position,makeASubItem(value),showAnimation);
    }

    public TextView makeASubItem(String value){
        TextView tv= new TextView(getContext());
        tv.setText(value);
        tv.setTextSize(20f);
        tv.setTextColor(textColor);
        tv.setPadding(paddingText,0,paddingText,0);
        tv.setGravity(Gravity.CENTER);
        LayoutParams childLp= new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        childLp.gravity= Gravity.CENTER;
        tv.setLayoutParams(childLp);
        return tv;
    }


    private void animateRemoveChild(int position) {
        YoYo.with(Techniques.FlipOutY).duration(200).onEnd(animator -> {
            numberFields.removeViewAt(position);
            isLock = false;
        }).playOn(numberFields.getChildAt(position));
    }

    private void animateAddChild(int position,View child,Boolean showAnimation) {
        numberFields.addView(child,position);
        if(showAnimation) {
        YoYo.with(Techniques.Bounce).duration(100).onEnd(animator->{
            isLock = false;
        }).playOn(child);
        } else {
            isLock = false;
        }
    }

    public int getChildSize(){
        return values.size();
    }

    public String getCallNumber(){
        StringBuilder sb=new StringBuilder();
        for(String value:values){
            sb.append(value);
        }
        return sb.toString();
    }


    public void reset(){
//        copyPasteView.hideAnimation();
        copyPasteView.hideWithOutAnimation();
        isCopyPasteViewShown = false;
        isLock = false;
        while(numberFields.getChildCount()>0){
            numberFields.removeViewAt(0);
        }
        values.clear();
    }

    private void enterCopyPasteMode(){
        copyPasteView.showAnimation();
        isCopyPasteViewShown=true;
    }

    private void exitCopyPasteMode(){
        copyPasteView.hideAnimation();
        isCopyPasteViewShown = false;
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, @Nullable Rect previouslyFocusedRect) {
        if(gainFocus==false && isCopyPasteViewShown) {
            exitCopyPasteMode();
            super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        }
    }
}
