package com.bisphone.interviewtest.custom;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bisphone.interviewtest.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class CallNumberCopyPasteView extends LinearLayout{
    int circleWidth = 0;
    Paint circlePaint = new Paint();
    private ButtonClickListener clickListener;

    public CallNumberCopyPasteView(Context context) {
        super(context);
        invalidateFields();
    }

    private void invalidateFields() {
        setWillNotDraw(false);
        circlePaint.setColor(Color.WHITE);

        View copy = makeTextView("Copy",ContextCompat.getColor(getContext(), R.color.md_cyan_300));
        copy.setOnClickListener(view->{
            if(clickListener!=null)
                clickListener.copyClicked();
        });
        copy.setVisibility(INVISIBLE);
        this.addView(copy);

        View paste = makeTextView("Paste",ContextCompat.getColor(getContext(), R.color.md_orange_300));
        paste.setOnClickListener(view->{
            if(clickListener!=null)
                clickListener.pasteClicked();
        });
        paste.setVisibility(INVISIBLE);
        this.addView(paste);

    }

    private TextView makeTextView(String text, int color) {
        TextView tv= new TextView(getContext());
        tv.setText(text);
        tv.setTextSize(20f);
        tv.setTextColor(color);
        tv.setGravity(Gravity.CENTER);
        LayoutParams childLp= new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        childLp.gravity= Gravity.CENTER;
        childLp.weight = 1;
        tv.setLayoutParams(childLp);
        return tv;
    }

    public CallNumberCopyPasteView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        invalidateFields();
    }

    public CallNumberCopyPasteView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        invalidateFields();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CallNumberCopyPasteView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        invalidateFields();
    }

    public void showAnimation(){
        ValueAnimator animator = ValueAnimator.ofInt(0,getWidth());
        animator.addUpdateListener(valueAnimator -> {
            circleWidth = (int)valueAnimator.getAnimatedValue();
            invalidate();
        });
        animator.setDuration(500);
        animator.start();
        showButtons();
    }

    private void showButtons() {
        getChildAt(0).setVisibility(VISIBLE);
        getChildAt(1).setVisibility(VISIBLE);
        YoYo.with(Techniques.FadeInDown).playOn(getChildAt(0));
        YoYo.with(Techniques.FadeInDown).playOn(getChildAt(1));
    }


    private void hideButtons() {
        getChildAt(0).setVisibility(INVISIBLE);
        getChildAt(1).setVisibility(INVISIBLE);
    }


    public void hideWithOutAnimation() {
        hideButtons();
        circleWidth =0;
        invalidate();
    }

    public void hideAnimation(){
        ValueAnimator animator = ValueAnimator.ofInt(getWidth(),0);
        animator.addUpdateListener(valueAnimator -> {
            circleWidth = (int)valueAnimator.getAnimatedValue();
            invalidate();
        });
        animator.setDuration(300);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                hideButtons();
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(getWidth()/2 , getHeight() / 2 ,circleWidth,circlePaint);
        super.onDraw(canvas);
    }

    public void setButtonClickListener(ButtonClickListener clickListener) {
        this.clickListener = clickListener;
    }


    interface ButtonClickListener{
        void copyClicked();
        void pasteClicked();
    }
}
