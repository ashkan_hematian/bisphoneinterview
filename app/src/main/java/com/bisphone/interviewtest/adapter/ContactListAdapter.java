package com.bisphone.interviewtest.adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bisphone.interviewtest.R;
import com.bisphone.interviewtest.model.Contact;
import com.bisphone.interviewtest.utility.ColorHelper;
import com.bisphone.interviewtest.utility.TextHelper;

import java.util.ArrayList;
import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactHolder>{

    private final List<Contact> items = new ArrayList<>();
    private final TextDrawable.IBuilder tBuilder;
    private final View.OnClickListener clickListener;

    public ContactListAdapter(View.OnClickListener listener) {
        this.clickListener = listener;
        tBuilder = TextDrawable.builder()
                .beginConfig()
                .height(60)
                .width(60)
                .textColor(Color.WHITE)
                .useFont(Typeface.DEFAULT)
                .fontSize(15)
                .bold()
                .toUpperCase()
                .endConfig()
                .round();
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_list,parent,false));
    }

    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        Contact item = items.get(position);
        holder.title.setText(item.getName());
        holder.phone.setText(item.getPhone());
        holder.textDrawable.setImageDrawable(tBuilder.build(TextHelper.getCircleDrawableContactName(item.getName()), ColorHelper.getColor(position)));
        if(clickListener!=null) {
            holder.wrapper.setTag(item);
            holder.wrapper.setOnClickListener(clickListener);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setNewData(List<Contact> newItems){
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    class ContactHolder extends RecyclerView.ViewHolder{

        protected ImageView textDrawable;
        protected TextView title;
        protected TextView phone;
        protected View wrapper;
        public ContactHolder(View itemView) {
            super(itemView);
            wrapper = itemView;
            title = itemView.findViewById(R.id.textView_item_contactList_title);
            phone = itemView.findViewById(R.id.textView_item_contactList_phone);
            textDrawable = itemView.findViewById(R.id.imageView_item_contactList_textIcon);
        }
    }
}
