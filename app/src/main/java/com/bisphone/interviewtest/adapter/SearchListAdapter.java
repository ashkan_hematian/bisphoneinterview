package com.bisphone.interviewtest.adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bisphone.interviewtest.R;
import com.bisphone.interviewtest.model.Contact;
import com.bisphone.interviewtest.utility.ColorHelper;

import java.util.ArrayList;
import java.util.List;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ContactHolder>{

    private final List<Contact> items = new ArrayList<>();
    private final TextDrawable.IBuilder tBuilder;
    private final View.OnClickListener clickListener;

    public SearchListAdapter(View.OnClickListener listener) {
        this.clickListener = listener;
        tBuilder = TextDrawable.builder()
                .beginConfig()
                .height(70)
                .width(70)
                .textColor(Color.WHITE)
                .useFont(Typeface.DEFAULT)
                .fontSize(12)
                .bold()
                .endConfig()
                .round();
    }


    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_list,parent,false));
    }

    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        Contact item = items.get(position);
        holder.textDrawable.setImageDrawable(tBuilder.build(item.getName()==null ? "" : item.getName(),ColorHelper.getColor(position)));
        if(clickListener!=null) {
            holder.wrapper.setTag(item);
            holder.wrapper.setOnClickListener(clickListener);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setNewData(List<Contact> newItems){
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    class ContactHolder extends RecyclerView.ViewHolder{

        protected ImageView textDrawable;
        protected View wrapper;
        public ContactHolder(View itemView) {
            super(itemView);
            wrapper = itemView;
            textDrawable = itemView.findViewById(R.id.imageView_item_SearchList_textIcon);
        }
    }
}
